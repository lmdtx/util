package com.stevezong.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.http.util.EntityUtils;

public class HttpClientUtil {
	/*
	 * public static void main(String[] args) throws ClientProtocolException,
	 * IOException { String url = "https://a.wunderlist.com/api/v1/lists";
	 * HttpClient client = new DefaultHttpClient();
	 * client.getParams().setParameter(CoreConnectionPNames.CONNECTION_TIMEOUT,
	 * 2000); client.getParams().setParameter(CoreConnectionPNames.SO_TIMEOUT,
	 * 2000); HttpPost post = new HttpPost(url); post.setHeader("X-Access-Token",
	 * "2894475ed2dd528cb7b06ddac61f7fbf595f09915ecda1e072c15fce08ca");
	 * post.setHeader("X-Client-ID", "04346b0c4ffb73c529ee");
	 * post.setHeader("Content-type", "application/json; charset=utf-8");
	 * //post.setHeader("Connection", "Close"); String sessionId = getSessionId();
	 * //post.setHeader("SessionId", sessionId); HttpResponse response =
	 * client.execute(post); int statusCode =
	 * response.getStatusLine().getStatusCode(); System.out.println(statusCode); }
	 * 
	 * public static String getSessionId(){ UUID uuid = UUID.randomUUID(); String
	 * str = uuid.toString(); return str.substring(0, 8) + str.substring(9, 13) +
	 * str.substring(14, 18) + str.substring(19, 23) + str.substring(24); }
	 */

	public String doPost(String url, Map<String, String> map, String charset) {
		CloseableHttpClient httpClient = null;
		HttpPost httpPost = null;
		String result = null;
		try {
			httpClient = HttpClients.createDefault();
			httpPost = new HttpPost(url);
			httpPost.setHeader("X-Access-Token", "2894475ed2dd528cb7b06ddac61f7fbf595f09915ecda1e072c15fce08ca");
			httpPost.setHeader("X-Client-ID", "04346b0c4ffb73c529ee");
			httpPost.setHeader("Content-type", "application/json");
			// 设置参数
			List<NameValuePair> list = new ArrayList<NameValuePair>();
			Iterator iterator = map.entrySet().iterator();
			while (iterator.hasNext()) {
				Map.Entry<String, String> elem = (Map.Entry<String, String>) iterator.next();
				list.add(new BasicNameValuePair(elem.getKey(), elem.getValue()));
			}
			if (list.size() > 0) {
				UrlEncodedFormEntity entity = new UrlEncodedFormEntity(list, charset);
				httpPost.setEntity(entity);
			}
			HttpResponse response = httpClient.execute(httpPost);
			System.out.println(response.getStatusLine().getStatusCode());
			if (response != null) {
				HttpEntity resEntity = response.getEntity();
				if (resEntity != null) {
					result = EntityUtils.toString(resEntity, charset);
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	public static void main(String[] args) {
		String url = "http://a.wunderlist.com/api/v1/lists";
		Map<String, String> params = new HashMap<String, String>();
		// params.put("page","100");
		//params.put("X-Access-Token", "2894475ed2dd528cb7b06ddac61f7fbf595f09915ecda1e072c15fce08ca");
		//params.put("X-Client-ID", "04346b0c4ffb73c529ee");
		HttpClientUtil client = new HttpClientUtil();
		String result = client.doPost(url, params, "UTF-8");
		System.out.println(result);
		// int codeId = response.getStatusLine().getStatusCode();
		 
	}

/*		public static void main(String[] args) throws IOException {
			CloseableHttpClient client = HttpClients.createDefault();
		    HttpPost httpPost = new HttpPost("https://a.wunderlist.com/api/v1/lists");
		    //httpPost.setHeader("Accept", "application/json");
		    httpPost.setHeader("Content-type", "application/json");
		 //   httpPost.setHeader(header);
		   // Header header =
			httpPost.setHeader("X-Access-Token", "2894475ed2dd528cb7b06ddac61f7fbf595f09915ecda1e072c15fce08ca");
			httpPost.setHeader("X-Client-ID", "04346b0c4ffb73c529ee");
			httpPost.
			//httpPost.setHeader("User-Agent", "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:45.0) Gecko/20100101 Firefox/45.0");
			HttpResponse response = client.execute(httpPost);
		    int codeId = response.getStatusLine().getStatusCode();
		    System.out.println(codeId);
		    HttpEntity resEntity = response.getEntity();
		    System.out.println(EntityUtils.toString(resEntity));
		   // assertThat(response.getStatusLine().getStatusCode(), equalTo(200));
		    client.close();
		}*/
	
}
