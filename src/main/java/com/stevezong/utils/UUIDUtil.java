package com.stevezong.utils;

import java.util.UUID;

public class UUIDUtil {
	public static String createId(){
		UUID uuid = UUID.randomUUID();
		return uuid.toString().replace("-", "");
		//return uuid.toString();
	}
}
