package com.stevezong.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.http.util.EntityUtils;

public class HttpClientGetUtil {

	public static void main(String[] args) throws IOException {
		CloseableHttpClient client = HttpClients.createDefault();
		//HttpPost httpPost = new HttpPost("https://a.wunderlist.com/api/v1/lists");
		HttpGet httpGet = new HttpGet("http://a.wunderlist.com/api/v1/lists");
		httpGet.setHeader("X-Client-ID", "04346b0c4ffb73c529ee");
		httpGet.setHeader("X-Access-Token", "42dae16779287240b2d33d8ea097e43c462e8b91f6112afb90bd63c6dc8d");
		HttpResponse response = client.execute(httpGet);
		int codeId = response.getStatusLine().getStatusCode();
		System.out.println(codeId);
		HttpEntity resEntity = response.getEntity();
		System.out.println(EntityUtils.toString(resEntity));
		client.close();
	}
	

}
