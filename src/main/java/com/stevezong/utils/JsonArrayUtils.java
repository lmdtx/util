package com.stevezong.utils;

import java.lang.reflect.Type;
import java.util.LinkedList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
/**
 * Gson解析json的纯数组
 * [{"name":"steve","age":"18"},{"name":"steve","age":"18"}]
 * 按照json 写一个bean �? 也就是泛�?<T>
 * @jsonStr 是json字符�?
 * @result 返回的是�?个list 
 * @<T>  
 * @author steve
 *
 */
public class JsonArrayUtils {
	public static <T> List<T> getList(String jsonStr){
		Type listType = new TypeToken<LinkedList<T>>(){}.getType();
		Gson gson = new Gson();
		LinkedList<T> result = gson.fromJson(jsonStr, listType);
		return result;
	}
}
