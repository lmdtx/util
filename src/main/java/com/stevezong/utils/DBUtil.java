package com.stevezong.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBUtil {

	public static Connection openConnection() throws Exception{
		Connection conn =null;
		try{
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/opsys?useUnicode=true&characterEncoding=utf8","root","hiroot");
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
		return conn;
	}
	
	public static void colse(Connection conn){
		try {
			if(conn != null){
				conn.close();
			}
				
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
